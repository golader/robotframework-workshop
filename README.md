# Robot Framework Workshop

## Getting Started

### Requirements
* Python >3 (https://www.python.org/downloads/) with pip
  * python directories in PATH (Windows default=C:\Python37, C:\Python37\Scripts)
  * https_proxy (URL of the proxy, if needed for installation of packages by pip)

### Install Robot Framework
Install robotframework with python package manager pip from console (maybe needs to be started as administrator)
```sh
pip install robotframework
```
alternatively install from source (https://code.google.com/archive/p/robotframework/downloads)
```sh
python setup.py install
```

### Choose integrated development environment (IDE)
Generally you are free in choosing your favorite IDE.  
Many developers prefer PyCharm since Robot Framework is python-based. There are also dedicated IDEs for Robot Framework. Some of them are listed below (see more on https://robotframework.org/#tools).  
Lately the language server protocol for Robot Framework has been developed and is continuously enhanced. Currently the plugin works best with Visual Studio Code.

#### Visual Studio Code (recommended)
* install VS Code (https://code.visualstudio.com/download)
* add plugins
  * 'Robot Framework Language Server' (https://hub.robocorp.com/knowledge-base/articles/language-server-protocol-for-robot-framework/)
    * configure plugin (settings.json - "robot.pythonpath":["</path/to/libraries>"]; launch.json)
    * press Crtl+F5 to execute open test suite
  * optional: install plugin for viewing html files (Report, Log)
    * Plugin name: view-in-browser
    * right click .html file and click "View in Browser" or "View in Other Browsers" and select your browser

#### PyCharm 
* install PyCharm (https://www.jetbrains.com/de-de/pycharm/download/#section=windows)
* add plugins
  * 'Robot Framework Support' and 'Intellibot' OR
  * 'Robot Framework Language Server' (https://forum.robotframework.org/t/pycharm-with-lsp/161)

#### RED (Eclipse based editor with a debugger by Nokia)
* install RED (https://github.com/nokia/RED)

#### RIDE (Standalone Robot Framework test data editor)
* install RIDE (https://github.com/robotframework/RIDE/wiki)

#### Eclipse
* install Eclipse (https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-12/R/eclipse-inst-win64.exe)
* install plugin 'RobotFramework-EclipseIDE' (https://github.com/NitorCreations/RobotFramework-EclipseIDE/wiki/Installation)
* optional: install PyDev (https://www.till.net/technologie/plone-zope-python/python-entwicklung-mit-eclipse)

## Documentation
* General: https://robotframework.org/robotframework/
* User Guide: http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#test-data-syntax
* Standard Library Documentation: https://robotframework.org/robotframework/
* Other Libraries: https://robotframework.org/#libraries